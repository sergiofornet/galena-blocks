<?php
/**
 * Plugin Name: galena-blocks
 * Plugin URI: https://bitbucket.org/sergiofornet/galena-blocks/
 * Description: galena-blocks — is a Gutenberg plugin created via create-guten-block.
 * Author: Betty Symington
 * Authot URI: https://betty-symington.xyz
 * Version: 0.2.4
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
