/**
 * Gutenberg Blocks
 *
 */

import './featured-text/featured-text.js';
import './content-img/content-img.js';
import './content-img-big/content-img-big.js'
import './grid/grid.js';
import './grid-half-left/grid-half-left.js';
import './grid-half-right/grid-half-right.js';
import './card/card.js';
import './subtitle/subtitle.js';