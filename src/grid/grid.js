/**
 * BLOCK: galena-blocks
 *
 * grid block
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

// Import galena img
import galenaImg from '../galena-img';

const { RichText, MediaUpload } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Button } = wp.components;

registerBlockType( 'cgb/block-galena-blocks-grid', {
	title: __( 'galena-grid' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'galena' ),
		__( 'grid' ),
		__( 'subtítulo' ),
	],
	attributes: {
		title: {
			source: 'text',
			selector: '.galena-grid__title'
		},
		body: {
			source: 'children',
			selector: '.galena-grid__body'
		},
		imageAlt: {
			attribute: 'alt',
			selector: '.galena-grid__image__img'
		},
		imageUrl: {
			attribute: 'src',
			selector: '.galena-grid__image__img'
		}
	},

	edit({ attributes, className, setAttributes }) {
		const getImageButton = (openEvent) => {
			if(attributes.imageUrl) {
				return (
					<img
						src={ attributes.imageUrl }
						onClick={ openEvent }
						className="editor__content-image"
					/>
				);
			}
			else {
				return (
					<div className="editor__button-container">
						<Button
							onClick={ openEvent }
							className="button button-large"
						>
							elige una imagen.
						</Button>
					</div>
				);
			}
		};
		return (
			<div className="editor-container__grid">
				<RichText
					onChange={ content => setAttributes({ title: content }) }
					value={ attributes.title }
					placeholder="escribe un título"
					tagName="h2"
					formattingControls="{ ['link'] }"
					isSelected={ attributes.isSelected }
					className="editor__title-text"
				/>
				<RichText
					onChange={ content => setAttributes({ body: content }) }
					value={ attributes.body }
					multiline="p"
					placeholder="escribe un texto que mole"
					formattingControls="{ ['bold', 'italic', 'link'] }"
					isSelected={ attributes.isSelected }
					className="editor__body-text"
				/>
				<MediaUpload
					onSelect={ media => { setAttributes({ imageAlt: media.alt, imageUrl: media.url }); } }
					type="image"
					value={ attributes.imageID }
					render={ ({ open }) => getImageButton(open) }
				/>
			</div>
		);
	},

	save({ attributes }) {
		return (
			<div className="galena-grid">
				<RichText.Content tagName="h2" className="galena-grid__title" value={ attributes.title } />
				<div className="galena-grid__image">
					{ galenaImg(attributes.imageUrl, attributes.imageAlt, 'galena-grid') }
				</div>
				<RichText.Content tagName="div" className="galena-grid__body" value={ attributes.body } />
			</div>
		);
	},
} );
