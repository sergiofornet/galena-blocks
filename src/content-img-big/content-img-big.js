/**
 * BLOCK: galena-blocks
 *
 * content-img-big block
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

// Import galena img
import galenaImg from '../galena-img';

const { MediaUpload } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Button } = wp.components;


registerBlockType( 'cgb/block-galena-blocks-image-big', {
	title: __( 'galena-imagen-grande' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'galena' ),
		__( 'imagen grande' ),
		__( 'foto' ),
	],
	attributes: {
		imageAlt: {
			attribute: 'alt',
			selector: '.galena-imagen__img-big'
		},
		imageUrl: {
			attribute: 'src',
			selector: '.galena-imagen__img-big'
		}
	},

	edit({ attributes, className, setAttributes }) {
		const getImageButton = (openEvent) => {
			if(attributes.imageUrl) {
			  return (
				<img
				  src={ attributes.imageUrl }
				  onClick={ openEvent }
				  className="editor__content-image-big"
				/>
			  );
			}
			else {
			  return (
				<div className="editor__button-container">
				  <Button
					onClick={ openEvent }
					className="button button-large"
				  >
					elige una imagen.
				  </Button>
				</div>
			  );
			}
		  };
		return (
			<div className="editor-container__content-img-big">
				<MediaUpload
					onSelect={ media => { setAttributes({ imageAlt: media.alt, imageUrl: media.url }); } }
					type="image"
					value={ attributes.imageID }
					render={ ({ open }) => getImageButton(open) }
				/>
			</div>
		);
	},

	save({ attributes }) {
		return (
			<div className="galena-imagen-big">
				{ galenaImg(attributes.imageUrl, attributes.imageAlt, 'galena-imagen-grande') }
            </div>
		);
	}
});
