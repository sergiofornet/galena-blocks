// Galena icon
const icon =
	<svg x="0px" y="0px" height="80" width="80"
	 viewBox="0 0 80 80">
		<polygon points="23.6,42.5 35.4,54.3 54.1,54.3 41,67.3 16.7,43 41.7,18 41.7,0.7 5.7,36.3 5.7,50.4 34.9,79.7 46.5,79.7 74,52.1
		74,42.5 "/>
	</svg>;

 export default icon;