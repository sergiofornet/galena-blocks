/**
 * BLOCK: galena-blocks
 *
 * content-img-tiny block
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

const { MediaUpload } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Button } = wp.components;


registerBlockType( 'cgb/block-galena-blocks-tiny-images', {
	title: __( 'galena-imágenes-pequeñas' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'galena' ),
		__( 'imagénes pequeñas' ),
		__( 'foto' ),
	],
	attributes: {
		imageAlt: {
			attribute: 'alt',
			selector: '.galena-imagen__img'
		},
		imageUrl: {
			attribute: 'src',
			selector: '.galena-imagen__img'
		}
	},

	edit({ attributes, className, setAttributes }) {
		const getImageButton = (openEvent) => {
			if(attributes.imageUrl) {
			  return (
				<img
				  src={ attributes.imageUrl }
				  onClick={ openEvent }
				  className="editor__content-image-tiny"
				/>
			  );
			}
			else {
			  return (
				<div className="editor__button-container">
				  <Button
					onClick={ openEvent }
					className="button button-large"
				  >
					elige una imagen.
				  </Button>
				</div>
			  );
			}
		  };
		return (
			<div className="editor-container__content-img">
				<MediaUpload
					onSelect={ media => { setAttributes({ imageAlt: media.alt, imageUrl: media.url }); } }
					type="image"
					value={ attributes.imageID }
					render={ ({ open }) => getImageButton(open) }
				/>
				<MediaUpload
					onSelect={ media => { setAttributes({ imageAlt: media.alt, imageUrl: media.url }); } }
					type="image"
					value={ attributes.imageID }
					render={ ({ open }) => getImageButton(open) }
				/>
			</div>
		);
	},

	save({ attributes }) {
		const galenaImg = (src, alt) => {
			if (!src) return null;

			if (alt) {
				return (
					<img
						className="galena-imagen-tiny__img-tiny"
						src={ src }
						alt={ alt }
					/>
				);
			}

			// No alt set, hide it from screen readers
			return (
				<img
					className="galena-imagen-tiny__img-tiny"
					src={ src }
					alt=""
					aria-hiden="true"
				/>
			);
		};

		return (
			<div className="">
				<div className="galena-imagen-tiny">
					{ galenaImg(attributes.imageUrl, attributes.imageAlt) }
				</div>
			</div>
		);
	}
});
