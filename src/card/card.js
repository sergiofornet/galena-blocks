/**
 * BLOCK: galena-blocks
 *
 * card block
 *
 * @since  0.2.0
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

// Import galena img
import galenaImg from '../galena-img';

const { RichText, MediaUpload } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Button } = wp.components;


registerBlockType( 'cgb/block-galena-blocks-card', {
	title: __( 'galena-card' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'texto' ),
		__( 'imagen' ),
		__( 'galena' ),
	],
	attributes: {
		title: {
			source: 'text',
			selector: '.galena-card__title'
		},
		body: {
			source: 'children',
			selector: '.galena-card__body'
		},
		imageAlt: {
			attribute: 'alt',
			selector: '.galena-card__img'
		},
		imageUrl: {
			attribute: 'src',
			selector: '.galena-card__img'
		}
	},

	edit({ attributes, className, setAttributes }) {
		const getImageButton = (openEvent) => {
			if(attributes.imageUrl) {
				return (
					<img
						src={ attributes.imageUrl }
						onClick={ openEvent }
						className="editor__content-image"
					/>
				);
			}
			else {
				return (
					<div className="editor__button-container">
						<Button
							onClick={ openEvent }
							className="button button-large"
						>
							elige una imagen.
						</Button>
					</div>
				);
			}
		};
		return (
			<div className="editor-container__card">
				<MediaUpload
					onSelect={ media => { setAttributes({ imageAlt: media.alt, imageUrl: media.url }); } }
					type="image"
					value={ attributes.imageID }
					render={ ({ open }) => getImageButton(open) }
				/>
				<RichText
					onChange={ content => setAttributes({ title: content }) }
					value={ attributes.title }
					placeholder="escribe un título"
					tagName="h2"
					isSelected={ attributes.isSelected }
					className="editor__title-text"
				/>
				<RichText
					onChange={ content => setAttributes({ body: content }) }
					value={ attributes.body }
					multiline="p"
					placeholder="escribe un texto"
					formattingControls="{ ['bold', 'italic', 'underline'] }"
					isSelected={ attributes.isSelected }
					className="editor__body-text"
				/>
			</div>
		);
	},

	save({ attributes }) {
		const title = attributes.title ?
			<RichText.Content tagName="h2" className="galena-card__title" value={ attributes.title } /> : null;
		return (
			<div className="galena-card">
				<div className="galena-card__image-container">
					<div className="galena-card__wrapper">
						<div className="galena-card__image">
							{ galenaImg(attributes.imageUrl, attributes.imageAlt, 'galena-card') }
						</div>
					</div>
				</div>
				<div className="galena-card__body-container">
					{ title }
					<RichText.Content tagName="div" className="galena-card__body" value={ attributes.body } />
				</div>
			</div>
		);
	},
} );
