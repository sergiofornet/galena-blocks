/**
 * BLOCK: galena-blocks
 *
 * Registering featured text block
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

const { RichText } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'cgb/block-galena-blocks-subtitle', {
	title: __( 'galena-subtítulo' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'galena' ),
		__( 'texto' ),
		__( 'subtítulo' ),
	],
	attributes: {
		subtitle: {
			source: 'text',
			selector: '.galena-subtitle__content'
		}
	},

	edit({ attributes, className, setAttributes }) {
		return (
			<div className="editor-container__subtitle">
				<RichText
					onChange={ content => setAttributes({ subtitle: content }) }
					value={ attributes.subtitle }
					placeholder="escribe un subtítulo"
					tagName="h2"
					isSelected={ attributes.isSelected }
					className="editor__subtitle-text"
				/>
			</div>
		);
	},

	save({ attributes }) {
		return (
			<RichText.Content tagName="h2" className="galena-subtitle__content" value={ attributes.subtitle } />
		);
	},
} );
