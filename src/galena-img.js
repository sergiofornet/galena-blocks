const galenaImg = (src, alt, classPrefix) => {
	if (!src) return null;

	if (alt) {
		return (
			<img
				className={ `${ classPrefix }__img` }
				src={ src }
				alt={ alt }
			/>
		);
	}

	// No alt set, hide it from screen readers
	return (
		<img
			className={ `${ classPrefix }__img` }
			src={ src }
			alt=""
			aria-hiden="true"
		/>
	);
};

export default galenaImg;