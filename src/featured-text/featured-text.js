/**
 * BLOCK: galena-blocks
 *
 * featured-text block
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import Galena icon
import icon from '../icon';

const { RichText } = wp.editor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'cgb/block-galena-blocks-destacado', {
	title: __( 'galena-destacado' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'galena' ),
		__( 'texto destacado' ),
		__( 'subtítulo' ),
	],
	attributes: {
		featured: {
			source: 'text',
			selector: '.galena-destacado__content'
		}
	},

	edit({ attributes, className, setAttributes }) {
		return (
			<div className="editor-container__featured-text">
				<RichText
					onChange={ content => setAttributes({ featured: content }) }
					value={ attributes.featured }
					placeholder="escribe un texto destacado."
					formattingControls="{ ['bold', 'italic', 'underline', 'link'] }"
					isSelected={ attributes.isSelected }
					className="editor__featured-text"
				/>
			</div>
		);
	},

	save({ attributes }) {
		return (
			<p className="galena-destacado__content">{ attributes.featured }</p>
		);
	},
} );
